# NFT2 Aggregator Web App
## Docker Dev Environment with local NGINX
```bash
docker run --rm  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm install --force && chmod -R 777 node_modules'
```
```bash
docker run --rm  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm run build'
```

```bash
docker-compose -f docker-compose-local.yaml up
```
http://localhost:8080/zilliqa

## Update node modules and build:

### `yarn`

### `yarn build`
