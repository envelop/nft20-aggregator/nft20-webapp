import { useParams } from "react-router-dom";
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { useGAPageTracker } from '../helpers/UseGAEventTracker';
import { ReactComponent as Bgimg } from '../pics/bg/bg-lg-right-6.svg';
import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

const InnerPage = () => {

    useGAPageTracker(window.location.pathname + window.location.search, "Inner page view");

    const inner_data = {
        "dao-envelop": {
            "title": "DAO Envelop is NFT2",
            "desc": "New-style programmable non-interchangeable tokens. From dNFTs to full-fledged wNFTs.",
            "h1": "Envelop - Protocol and Oracle for NFT 2.0",
            "body": '<p>DAO Envelop was conceived at the end of 2020 and made its debut in 2021, incorporating innovative trends from various segments of Web 3.0: DeFi, GameFi, NFT, and more.</p><p>Today, Envelop is a protocol that operates with on-chain data, an oracle that works with both on-chain and off-chain data, and an index that can be thought of as a synthetic product (service) based on the capabilities of the protocol and oracle.</p><p>For example, with the Envelop DAO Oracle and Protocol, you can create a SAFT - a multi-wrapped instrument of interchangeable and non-interchangeable tokens that can be used in the OTC market, for instance, when tokens have been released by a project (TGE), but have not yet entered the open market (IDO/INO/ITO, etc.).</p><p>Envelop has given rise to micro-DAOs such as <a href="https://bridgeless.io/" rel="nofollow noreferrer" target="_blank">Bridgeless</a>, a peer-to-peer platform for token exchange via wNFT collateral, <a href="https://getpass.is/" rel="nofollow noreferrer" target="_blank">Getpass</a>, a service based on the Proof-of-Event concept, and several others: <a href="https://scaleswap.io/wnft/" rel="nofollow noreferrer" target="_blank">ScaleSwap wNFT Launchpad</a>, LottTrade wNFT game, <a href="https://scotch.sale/" rel="nofollow noreferrer" target="_blank">Scotch</a> - OTC platform, and more.</p><p>Envelop operates across different EVM networks: Ethereum, BNB chain (BSC), Polygon (MATIC), Aurora, Harmony, Avalanche EVM, Arbitrum, zkSync, etc.; as well as non-EVM networks: Near, Zilliqa, WAX, and is preparing to integrate with Resilu and Aptos.</p><p>For more information about the DAO, you can visit the page at <a href="https://daoenvelop.eth.limo/" rel="noreferrer" target="_blank">daoenvelop.eth.limo</a>, and for applications, check out <a href="https://app.envelop.is/" rel="noreferrer" target="_blank">app.envelop.is</a>.</p>'
        },
        "abacus": {
            "title": "Abacus",
            "desc": "Abacus is using free market principles to build financial infrastructure for NFTs on the Ethereum blockchain starting in 2021. ",
            "h1": "Abacus",
            "body": "Abacus is using free market principles to build financial infrastructure for NFTs on the Ethereum blockchain starting in 2021. Acting as a lender or borrower, Abacus allows users to value NFTs by strategically placing liquidity in different price bands, enabling them to generate yield and acquire exposure to NFTs. Borrowers are offered favorable terms such as low interest rate, high LTV and convenient liquidation terms."
        },
        "404": {
            "title": "404 Not found",
            "desc": "",
            "h1": "404 Not found",
            "body": "No content"
        }
    };

    let { name } = useParams();
    if(Object.keys(inner_data).indexOf(name) === -1) {
        name = "404";
    }

    return(
        <>
            <Helmet>
                <title>{inner_data[name].title}</title>
                <meta name="description" content={inner_data[name].desc} />
            </Helmet>
            <main className="s-main">
                <section className="sec-intro">
                    <div className="bg-gradient"></div>
                    <Bgimg className="sec-bg bg-right d-none d-xl-block" alt="" />
                    <div className="container">
                    <div className="row">
                        <div className="col-lg-12 pb-lg-4">
                            <Link to={"/"} className="back-link btn btn-gray">
                            &larr; Back to Main page
                            </Link>
                        </div>
                        <div className="col-lg-10 col-sm-12">
                            <h1>{inner_data[name].h1}<span className="text-grad d-block"></span></h1>
                            <div dangerouslySetInnerHTML={{__html:inner_data[name].body}}></div>
                        </div>
                    </div>
                    </div>
                </section>
            </main>
        </>
    )
};

export default InnerPage;